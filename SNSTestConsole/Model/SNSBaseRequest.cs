﻿using SNSWrapper.Interface.Model;

namespace Api.Models
{
    public abstract class SNSBaseRequest
    {
        public string region { get; set; }
        public string accountId { get; set; }
        public string subject { get; set; }
    }
}