﻿using SNSWrapper.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SNSWrapper.Interface.Model;
namespace Api.Models
{
    public class SNSMessage : ISNSDeploymentMessage
    {
        public string DeploymentName { get; set; }

        public string Urls { get; set; }

        public string Username { get; set; }
    }
}