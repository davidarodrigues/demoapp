﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SNSWrapper.Interface.Model;

namespace Api.Models
{
    public class SNSTopicCreateRequest : SNSBaseRequest, IPublishRequest
    {
        public SNSTopicCreateRequest() : base() { }
        public string subscriberEndoint { get; set; }
        public string endpointType { get; set; }
    }
}