﻿using System;
using SNSTestConsole.Interface;
using SNSTestConsole.Service;
using SNSWrapper.Interface.Service;
using SNSWrapper.Interface.Model;
using SNSWrapper.Model;
using SNSWrapper.Service;
using Amazon.Runtime;

namespace SNSTestConsole
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Starting Application");
                IOptions options = new Options(args);
                IPublisher publisher = null;
                IWorker worker = null;
                IArnGenerator arnGenerator = new DefaultArnGenerator();
                IPublisher helpPublisher = new HelpPublisher(options);
                var mapper = new Mapper<AmazonWebServiceResponse, SNSResponse>();
                IHandlerFactory handlerFactory = new HandlerFactory(options, mapper);
                ISNSHandler handler = handlerFactory.GetHandler();
                IPublisherFactory factory = new PublisherFactory(options, arnGenerator, helpPublisher, handler);

                publisher = factory.GetPublisher();

                worker = new Worker(publisher);
                worker.DoWork();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            
            EndProcess();
        }

        private static void EndProcess()
        {
            Console.WriteLine("Press any key to continue.");
            Console.ReadKey();
        }
    }
}
