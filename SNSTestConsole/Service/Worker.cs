﻿using Amazon.Runtime;
using SNSWrapper.Interface.Model;
using SNSWrapper.Interface.Service;
using SNSTestConsole.Interface;
using System;

namespace SNSTestConsole.Service
{
    public class Worker : IWorker
    {
        private IPublisher _publisher;
        public Worker(IPublisher publisher)
        {
            _publisher = publisher;
        }
        public void DoWork()
        {
            try
            {
                if (_publisher.HasMissingParameters())
                {
                    Console.WriteLine("Missing required pararmeter(s).");
                    return;
                }

                Console.WriteLine(string.Format("Publishing {0} {1}", _publisher.Options.Type, _publisher.Options.Subject));

                IAmazonWebserviceResponse response = _publisher.Publish();

                if (response == null)
                {
                    Console.WriteLine(string.Format("The {0} already exists {1}.", _publisher.Options.Type, _publisher.Options.Subject));
                    return;
                }
                Console.WriteLine(string.Format("Create {0} returned code {1}", _publisher.Options.Type, response.HttpStatusCode.ToString()));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
