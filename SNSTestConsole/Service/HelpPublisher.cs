﻿using System;
using Amazon.Runtime;
using SNSWrapper.Interface.Model;
using SNSWrapper;
using SNSWrapper.Model;
using SNSWrapper.Publisher;

namespace SNSTestConsole.Service
{
    public class HelpPublisher : PublisherBase
    {
        public HelpPublisher(IOptions options) : base(options, null, null) { }

        public override IAmazonWebserviceResponse Publish()
        {
            foreach (var option in _options.OptionSet)
            {
                Console.WriteLine(option.Description);
            }
            return new SNSResponse() { HttpStatusCode = System.Net.HttpStatusCode.OK };
        }
    }
}
