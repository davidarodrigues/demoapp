﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Options
{
    public class ReportOptions
    {
        public enum CaseOption
        {
            None,
            Upper,
            Lower,
            Proper,
            Default
        }

        public bool FormatBody { get { return BodyOption != CaseOption.None; } }

        public bool FormatTitle { get { return TitleOption != CaseOption.None; } }

        public CaseOption BodyOption { get; set; }

        public CaseOption TitleOption { get; set; }
    }
}