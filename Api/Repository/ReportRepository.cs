﻿using Api.Framework;
using Api.Interface;
using System;

namespace Api.Repository
{
    public class ReportRepository<F> : BaseRepository<F>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReportRepository{F}"/> class.
        /// </summary>
        /// <param name="framework">The framework.</param>
        public ReportRepository(IFramework<F> framework)
            : base(framework)
        { }

        /// <summary>
        /// Gets the by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public override F GetById(int id)
        {
            try
            {
                F dbReport = _framework.GetById(id);

                if (dbReport == null)
                    return default(F);

                return dbReport;
            }
            catch (Exception e)
            {
                // Log error
                throw e;
            }
        }
    }
}