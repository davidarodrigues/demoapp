﻿using Api.Interface;

namespace Api.Repository
{
    public abstract class BaseRepository<F> : IRepository<F>
    {
        protected IFramework<F> _framework;

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseRepository{F}"/> class.
        /// </summary>
        /// <param name="framework">The framework.</param>
        public BaseRepository(IFramework<F> framework)
        {
            _framework = framework;
        }

        public abstract F GetById(int id);
    }
}