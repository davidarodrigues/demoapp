﻿using Api.Options;
using Api.Models;
using System;
using Api.Interface;

namespace Api.Formatter
{
    public class BodyFormatter : BaseFormatter
    {
        public BodyFormatter(ReportOptions options, IFormatHelper helper)
            : base(options, helper)
        {

        }

        public override void Format(ref IReport report)
        {
            try
            {
                switch (_options.BodyOption)
                {
                    case ReportOptions.CaseOption.Upper:
                        report.Body = report.Body.ToUpper();
                        break;
                    case ReportOptions.CaseOption.Lower:
                        report.Body = report.Body.ToLower();
                        break;
                    case ReportOptions.CaseOption.Proper:
                        report.Body = _formatHelper.FormatProperCase(report.Body);
                        break;
                    case ReportOptions.CaseOption.Default:
                        report.Body = _formatHelper.FormatDefault(report.Body);
                        break;
                }
            }
            catch (Exception e)
            {
                // Log
                throw e;
            }
        }
    }
}