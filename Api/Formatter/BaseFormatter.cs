﻿using Api.Interface;
using Api.Models;
using Api.Options;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace Api.Formatter
{
    public abstract class BaseFormatter : IReportFormatter
    {
        protected ReportOptions _options;
        protected IFormatHelper _formatHelper;
        public ReportOptions Options()
        {
            return _options;
        }
        public BaseFormatter(ReportOptions options, IFormatHelper helper)
        {
            _options = options;
            _formatHelper = helper;
        }

        public abstract void Format(ref IReport report);


    }
}