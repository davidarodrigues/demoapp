﻿using Api.Options;
using Api.Models;
using System;
using Api.Interface;

namespace Api.Formatter
{
    public class TitleFormatter :BaseFormatter
    {
        public TitleFormatter(ReportOptions options, IFormatHelper helper)
            : base(options, helper)
        {

        }

        public override void Format(ref IReport report)
        {
            try
            {
                switch (_options.TitleOption)
                {
                    case ReportOptions.CaseOption.Upper:
                        report.Title = report.Title.ToUpper();
                        break;
                    case ReportOptions.CaseOption.Lower:
                        report.Title = report.Title.ToLower();
                        break;
                    case ReportOptions.CaseOption.Proper:
                        report.Title = _formatHelper.FormatProperCase(report.Title);
                        break;
                }
            }
            catch(Exception e)
            {
                // log
                throw e;
            }
        }
    }
}