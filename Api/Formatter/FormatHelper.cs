﻿using Api.Interface;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace Api.Formatter
{
    public class FormatHelper : IFormatHelper
    {
        public string FormatProperCase(string value)
        {
            try
            {
                TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
                return textInfo.ToTitleCase(value);
            }
            catch (Exception e)
            {
                // Log
                throw e;
            }
        }

        public string FormatDefault(string value)
        {
            string[] sentences = value.Split('.');
            for (int i = 0; i < sentences.Count(); i++)
            {
                string sentence = sentences[i];
                string[] words = sentence.Split(' ');
                bool foundFirstWord = false;

                int index = -1;
                if (Regex.Match(sentence, "\\r\\n").Index > 0)
                    index = sentence.IndexOf("\r\n") - 1;

                for (int w = 0; w < words.Count(); w++)
                {
                    if (foundFirstWord)
                        break;
                    if (string.IsNullOrEmpty(words[w]))
                        continue;

                    foundFirstWord = true;
                    words[w] = FormatProperCase(words[w]);
                    break;
                }
                sentence = string.Join(" ", words);

                sentences[i] = sentence.Trim();
            }
            return string.Join(".  ", sentences).Trim();
        }
    }
}