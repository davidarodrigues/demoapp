﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Api.Models;
using Api.Service;
using Api.Interface;
using System.Web;
using System.Web.Http.Controllers;
using System.Collections.Specialized;
using Api.Repository;
using Api.Framework;
using Api.Filter;
using Api.Formatter;

namespace Api.Controllers
{
    [RoutePrefix("api/Reports")]
    public class ReportsController : ApiController
    {
        [HttpGet]
        [Route("{id:int}")]
        [DefaultFilter]
        public IHttpActionResult GetReport(int id)
        {
            try
            {
                IReportGenerator generator = (IReportGenerator)Request.Properties["generator"];

                IReport report = generator.GenerateReport(id);

                if (report == null)
                    return NotFound();

                return Ok(report);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        [HttpGet]
        [Route("{id:int}/details")]
        [DetailFilter]
        public IHttpActionResult GetReportDetails(int id)
        {
            try
            {
                IService<ReportDbSet, ReportDetail> service = (IService<ReportDbSet, ReportDetail>)Request.Properties["service"];
                IReportDetail report = service.GetById(id);

                if (report == null)
                    return NotFound();
                report.Title = new FormatHelper().FormatProperCase(report.Title);
                return Ok(report);
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }
    }
}
