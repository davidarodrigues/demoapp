﻿using System.Collections.Generic;
using System.Web.Http;
using Api.Models;
using SNSWrapper;
using Newtonsoft.Json;
using Amazon.SimpleNotificationService.Model;
using SNSWrapper.Interface.Model;
using SNSWrapper.Interface.Service;
using SNSWrapper.Service;
using SNSWrapper.Model;
using Api.Service;
using SNSWrapper.SNS;
using Amazon.Runtime;

namespace Api.Controllers
{
    [RoutePrefix("api/CSP")]
    public class CloudSuitePortalController : ApiController
    {

        [Route("publish/topic")]
        [HttpPost]
        public IHttpActionResult PublishTopic([FromBody] SNSTopicCreateRequest request)
        {
            string[] args = new string[] 
            { 
                AddArg("t", "topic"), 
                AddArg("a", request.accountId),
                AddArg("r", request.region),
                AddArg("s", request.subject)
            };
            return DoWork(args);
        }

        [Route("publish/message")]
        [HttpPost]
        public IHttpActionResult PublishMessage([FromBody] SNSMessageCreateRequest request)
        {
            string[] args = new string[] 
            { 
                AddArg("t", "message"), 
                AddArg("a", request.accountId),
                AddArg("r", request.region),
                AddArg("s", request.subject),
                AddArg("d", request.deploymentName),
                AddArg("u", request.urls),
                AddArg("n", request.username)
            };
            return DoWork(args);
        }

        [Route("subscribe")]
        [HttpPost]
        public IHttpActionResult Subscribe([FromBody] SNSSubscriptionRequest request)
        {
            string[] args = new string[] 
            { 
                AddArg("t", "subscribe"), 
                AddArg("a", request.accountId),
                AddArg("r", request.region),
                AddArg("s", request.subject),
                AddArg("e", request.endpoint),
                AddArg("p", request.protocol)
            };
            return DoWork(args);
        }

        private IHttpActionResult DoWork(string[] args)
        {
            IOptions options = new SNSWrapper.Model.Options(args);
            IArnGenerator arnGenerator = new DefaultArnGenerator();
            IPublisher publisher = null;
            IPublisher helpPublisher = new HelpPublisher(options);
            var mapper = new Mapper<AmazonWebServiceResponse, SNSResponse>();
            IHandlerFactory handlerFactory = new HandlerFactory(options, mapper);
            ISNSHandler snsHandler = handlerFactory.GetHandler();
            IPublisherFactory factory = new PublisherFactory(options, arnGenerator, helpPublisher, snsHandler);

            publisher = factory.GetPublisher();

            var worker = new SNSWorker(publisher);
            return Ok(worker.DoWork());
        }

        private string AddArg(string identifier, string value)
        {
            if (string.IsNullOrEmpty(value))
                return string.Empty;

            return string.Format("-{0}{1}", identifier, value);
        }
    }
}
