﻿using Api.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Models
{
    public class Report : IReport
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Color { get; set; }
        public string Body { get; set; }
    }
}