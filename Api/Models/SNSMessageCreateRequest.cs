﻿using SNSWrapper.Interface.Model;

namespace Api.Models
{
    public class SNSMessageCreateRequest : SNSBaseRequest, IPublishRequest
    {
        public string deploymentName { get; set; }
        public string urls { get; set; }
        public string username { get; set; }
    }
}