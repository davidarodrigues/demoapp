﻿using SNSWrapper.Interface.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Models
{
    public class SNSSubscriptionRequest : SNSBaseRequest
    {
        public string endpoint { get; set; }
        public string protocol { get; set; }

        public SNSSubscriptionRequest() : base() { }
    }
}