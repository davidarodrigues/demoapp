﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Api.Interface;

namespace Api.Models
{
    public class ReportDetail : IReportDetail
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Author { get; set; }
        public DateTime PublishedOn { get; set; }
        public int PageCount { get; set; }
    }
}