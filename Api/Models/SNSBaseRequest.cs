﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Models
{
    public abstract class SNSBaseRequest
    {
        public string region { get; set; }

        public string accountId { get; set; }

        public string subject { get; set; }
    }
}