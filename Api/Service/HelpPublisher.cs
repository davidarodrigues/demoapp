﻿using Amazon.Runtime;
using SNSWrapper;
using SNSWrapper.Interface.Model;
using SNSWrapper.Model;
using SNSWrapper.Publisher;

namespace Api.Service
{
    public class HelpPublisher : PublisherBase
    {
        public HelpPublisher(IOptions options) : base(options, null, null) { }
        public override IAmazonWebserviceResponse Publish()
        {
            var response = new SNSResponse();

            foreach (var option in _options.OptionSet)
            {
                response.MetaData.Add(string.Join(",", option.GetNames()), option.Description);
            }
            response.HttpStatusCode = System.Net.HttpStatusCode.OK;

            return response;
        }
    }
}