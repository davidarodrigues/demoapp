﻿using Api.Formatter;
using Api.Framework;
using Api.Interface;
using Api.Models;
using Api.Options;

namespace Api.Service
{
    public class ReportGenerator : IReportGenerator
    {

        private IService<ReportDbSet, IReport> _service;
        private IReportFormatter[] _formatters;

        /// <summary>
        /// Initializes a new instance of the <see cref="ReportGenerator"/> class.
        /// </summary>
        /// <param name="formatters">The formatters.</param>
        /// <param name="service">The service.</param>
        public ReportGenerator(IReportFormatter[] formatters, IService<ReportDbSet, IReport> service)
        {
            _formatters = formatters;
            _service = service;
        }

        /// <summary>
        /// Generates the report.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public IReport GenerateReport(int id)
        {
            IReport report = _service.GetById(id);

            if (report == null || _formatters == null)
                return report;

            foreach (IReportFormatter formatter in _formatters)
            {
                formatter.Format(ref report);
            }

            return report;
        }
    }
}