﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Api.Interface;
using Api.Models;
using SNSWrapper.Interface.Service;

namespace Api.Service
{
    public class ReportService<F, T> : BaseService<F, T>
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="ReportService{F, T}"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        /// <param name="mapper">The mapper.</param>
        public ReportService(IRepository<F> repository, IMapper<F, T> mapper) : base(repository, mapper) { }

        /// <summary>
        /// Gets the object by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        public override T GetById(int id)
        {
            F dbReport = _repository.GetById(id);
            T report;
            _mapper.Map(dbReport, out report);

            return report;
        }
    }
}