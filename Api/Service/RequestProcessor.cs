﻿using Api.Formatter;
using Api.Interface;
using Api.Options;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;

namespace Api.Service
{
    public class RequestProcessor : IRequestProcessor
    {
        /// <summary>
        /// Gets the formatters.
        /// </summary>
        /// <param name="queryString">The query string.</param>
        /// <returns></returns>
        public IReportFormatter[] GetFormatters(IEnumerable<KeyValuePair<string, string>> queryString)
        {
            
            List<IReportFormatter> formats = new List<IReportFormatter>();

            if (!queryString.Any())
                return formats.ToArray();

            KeyValuePair<string, string> bodyKvp = queryString.Where(k => k.Key == "body").First();

            string bodyFormat = bodyKvp.Value;
            string titleFormat = queryString.Where(k => k.Key == "title").First().Value;

            if (!string.IsNullOrEmpty(bodyFormat))
                formats.Add(GetFormatter("body", bodyFormat));

            if (!string.IsNullOrEmpty(titleFormat))
                formats.Add(GetFormatter("title", titleFormat));

            return formats.ToArray();
        }

        private IReportFormatter GetFormatter(string type, string formatName)
        {
            ReportOptions options = new ReportOptions();
            switch (type)
            {
                case "body":
                    return GetBodyFormatter(ref options, formatName);
                case "title":
                    return GetTitleFormatter(ref options, formatName);
                default:
                    return null;
            }
        }

        private IReportFormatter GetBodyFormatter(ref ReportOptions options, string formatName)
        {
            options.BodyOption = GetOptionFromString(formatName);
            return new BodyFormatter(options, new FormatHelper());
        }

        private IReportFormatter GetTitleFormatter(ref ReportOptions options, string formatName)
        {
            options.TitleOption = GetOptionFromString(formatName);
            return new TitleFormatter(options, new FormatHelper());
        }

        private ReportOptions.CaseOption GetOptionFromString(string name)
        {
            ReportOptions.CaseOption option = ReportOptions.CaseOption.None;
            Enum.TryParse(name, true, out option);
            return option;
        }
    }
}