﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Api.Interface;
using Api.Models;
using SNSWrapper.Interface.Service;

namespace Api.Service
{
    public abstract class BaseService<F, T> : IService<F, T>
    {
        protected IRepository<F> _repository;
        protected IMapper<F, T> _mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="BaseService{F, T}"/> class.
        /// </summary>
        /// <param name="repository">The repository.</param>
        /// <param name="mapper">The mapper.</param>
        public BaseService(IRepository<F> repository, IMapper<F, T> mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public abstract T GetById(int id);
    }
}