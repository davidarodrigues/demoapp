﻿using Amazon.Runtime;
using SNSWrapper.Interface.Model;
using SNSWrapper.Interface.Service;
using SNSWrapper.Model;
using System;

namespace Api.Service
{
    public class SNSWorker
    {
         private IPublisher _publisher;
        public SNSWorker(IPublisher publisher)
        {
            _publisher = publisher;
        }

        public IAmazonWebserviceResponse DoWork()
        {
            IAmazonWebserviceResponse response = new SNSResponse();
            try
            {
                if (_publisher.HasMissingParameters())
                {
                    response.MetaData.Add("Error", "Missing required pararmeter(s).");
                    return response;
                }

                response = _publisher.Publish();

                if (response == null)
                {
                    response.MetaData.Add("Error", string.Format("The {0} already exists {1}.", _publisher.Options.Type, _publisher.Options.Subject));
                    return response;
                }
                response.MetaData.Add("success", string.Format("Create {0} returned code {1}", _publisher.Options.Type, response.HttpStatusCode.ToString()));
            }
            catch (Exception e)
            {
                response.MetaData.Add("Exception", e.Message);
            }
            return response;
        }
    }
}