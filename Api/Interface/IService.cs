﻿
namespace Api.Interface
{
    public interface IService<F, T>
    {
        T GetById(int id);
    }
}
