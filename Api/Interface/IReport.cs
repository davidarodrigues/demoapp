﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Interface
{
    public interface IReport
    {
        int Id { get; set; }
        string Title { get; set; }
        string Color { get; set; }
        string Body { get; set; }
    }
}
