﻿using Api.Options;
using Api.Models;

namespace Api.Interface
{
    public interface IReportFormatter
    {
        ReportOptions Options();
        void Format(ref IReport report);
    }
}
