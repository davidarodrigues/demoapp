﻿using System.Collections.Generic;

namespace Api.Interface
{
    public interface IRequestProcessor
    {
        IReportFormatter[] GetFormatters(IEnumerable<KeyValuePair<string, string>> queryString);
    }
}
