﻿using Api.Models;
using Api.Options;

namespace Api.Interface
{
    public interface IReportGenerator
    {
        IReport GenerateReport(int id);
    }
}
