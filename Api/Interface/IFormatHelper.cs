﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Interface
{
    public interface IFormatHelper
    {
        string FormatProperCase(string value);
        string FormatDefault(string value);
    }
}
