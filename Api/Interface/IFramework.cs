﻿
namespace Api.Interface
{
    public interface IFramework<T>
    {
        T GetById(int id);
    }
}
