﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Api.Interface
{
    public interface IReportDetail
    {
        int Id { get; set; }
        string Title { get; set; }
        string Author { get; set; }
        DateTime PublishedOn { get; set; }
        int PageCount { get; set; }
    }
}
