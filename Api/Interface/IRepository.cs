﻿using Api.Models;

namespace Api.Interface
{
    public interface IRepository<F>
    {
        F GetById(int id);
    }
}
