﻿using Api.Framework;
using Api.Interface;
using Api.Models;
using Api.Repository;
using Api.Service;
using SNSWrapper.Interface.Service;
using SNSWrapper.Service;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web;
using System.Web.Http.Filters;

namespace Api.Filter
{
    public class DetailFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            IFramework<ReportDbSet> framework = new ReportFramework();

            IMapper<ReportDbSet, ReportDetail> mapper = new Mapper<ReportDbSet, ReportDetail>();

            IRepository<ReportDbSet> repository = new ReportRepository<ReportDbSet>(framework);

            IService<ReportDbSet, ReportDetail> service = new ReportService<ReportDbSet, ReportDetail>(repository, mapper);
            actionContext.Request.Properties.Add("service", service);
        }
    }
}