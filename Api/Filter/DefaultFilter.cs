﻿using Api.Framework;
using Api.Interface;
using Api.Models;
using Api.Repository;
using Api.Service;
using SNSWrapper.Interface.Service;
using SNSWrapper.Service;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web;
using System.Web.Http.Filters;

namespace Api.Filter
{
    public class DefaultFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            IFramework<ReportDbSet> framework = new ReportFramework();
            actionContext.Request.Properties.Add("framework", framework);

            IMapper<ReportDbSet, IReport> mapper = new Mapper<ReportDbSet, IReport>();

            IRepository<ReportDbSet> repository = new ReportRepository<ReportDbSet>(framework);
            actionContext.Request.Properties.Add("repository", repository);

            IService<ReportDbSet, IReport> service = new ReportService<ReportDbSet, IReport>(repository, mapper);
            actionContext.Request.Properties.Add("service", service);

            IReportGenerator generator = new ReportGenerator(GetFormatters(actionContext), service);
            actionContext.Request.Properties.Add("generator", generator);
        }

        private IReportFormatter[] GetFormatters(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            Uri request = actionContext.Request.RequestUri;

            List<KeyValuePair<string, string>> query = new List<KeyValuePair<string, string>>();
            NameValueCollection collection = HttpUtility.ParseQueryString(request.Query);

            if (collection.Count == 0)
            {
                actionContext.Request.Properties.Add("formatters", null);
                return null;
            }

            query.Add(new KeyValuePair<string, string>("body", collection.Get("body")));
            query.Add(new KeyValuePair<string, string>("title", collection.Get("title")));

            return new RequestProcessor().GetFormatters(query);
        }
    }
}