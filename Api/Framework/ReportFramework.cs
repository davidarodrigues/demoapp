﻿using Api.Models;
using Api.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Framework
{
    public class ReportFramework : IFramework<ReportDbSet>
    {
        public ReportDbSet GetById(int id)
        {
            IEnumerable<ReportDbSet> reports = Reports().Where(r => r.Id == id);
            if(!reports.Any())
                return null;

            return reports.First();
        }

        private IEnumerable<ReportDbSet> Reports()
        {
            List<ReportDbSet> reports = new List<ReportDbSet>();
            var body = @"lorem ipsum dolor sit amet, consectetur adipiscing elit. 
donec et venenatis orci. ut accumsan lectus risus. vestibulum porttitor efficitur ipsum commodo scelerisque. 
etiam auctor libero non turpis fermentum, eget eleifend dolor efficitur. lorem ipsum dolor sit amet, consectetur adipiscing elit. 
maecenas eu lacinia tellus. nunc ut quam malesuada, bibendum eros vel, posuere diam. nulla egestas mollis porttitor. 
phasellus luctus erat nec neque feugiat, a auctor lectus rutrum. aenean eget ultricies enim. 
fusce aliquet libero at est scelerisque dictum. phasellus vestibulum rutrum turpis. curabitur et faucibus dui, nec fermentum dolor. 
mauris tempor condimentum lacus gravida tempor. praesent vulputate elementum erat sed iaculis. 
maecenas nec ultricies quam. maecenas hendrerit nec ipsum quis porta.";
            var report1 = new ReportDbSet()
            {
                Id = 1,
                Title = "report one",
                Color = "pink",
                Body = body
            };
            reports.Add(report1);
            return reports.AsEnumerable();
        }
    }
}