﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Framework
{
    public class ReportDbSet
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Color { get; set; }
        public string Body { get; set; }
        public string Author { get; set; }
        public DateTime PublishedOn { get; set; }
        public int PageCount { get; set; }

        public ReportDbSet()
        {
            Author = "Doctor Eliot Doolittle";
            PublishedOn = new DateTime(1962, 1, 13);
            PageCount = 353;
        }
    }
}