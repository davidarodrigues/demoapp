﻿using Amazon.Runtime;
using Amazon.SimpleNotificationService.Model;
using SNSWrapper.Interface.Model;
using SNSWrapper.Interface.Service;
using SNSWrapper.Model;
using SNSWrapper.SNS;

namespace SNSWrapper.Publisher
{
    public class Subscriber : PublisherBase
    {
        public Subscriber(IOptions options, IArnGenerator topicArnGenerator, ISNSHandler snsHandler) : base(options, topicArnGenerator, snsHandler) 
        {
            _requiredParameters = new string[] { _options.AccountId, _options.Region, _options.Subject, _options.Endpoint, _options.Protocol };
        }
        public override IAmazonWebserviceResponse Publish()
        {
            IAmazonWebserviceResponse response = null;
            using (_snsHandler)
            {
                response = _snsHandler.Publish(_topicArn, _options.Endpoint, _options.Protocol);
            }
            return response;
        }
    }
}
