﻿using Amazon.Runtime;
using Amazon.SimpleNotificationService.Model;
using SNSWrapper.Interface.Model;
using SNSWrapper.Interface.Service;
using SNSWrapper.Model;
using SNSWrapper.SNS;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace SNSWrapper.Publisher
{
    public class MessagePublisher : PublisherBase
    {
        public MessagePublisher(IOptions options, IArnGenerator arnGenerator, ISNSHandler snsHandler) : base(options, arnGenerator, snsHandler) 
        {
            _requiredParameters = new string[] { _options.AccountId, _options.Region, _options.Subject, _options.DeploymentName, _options.Urls, _options.Username };
        }
        public override IAmazonWebserviceResponse Publish()
        {
            IAmazonWebserviceResponse response = null;
            using (_snsHandler)
            {
                SNSMessage message = new SNSMessage()
                {
                    DeploymentName = _options.DeploymentName,
                    Urls = _options.Urls,
                    Username = _options.Username
                };
                Dictionary<string, string> awsMessage = new Dictionary<string, string>() { { "default", JsonConvert.SerializeObject(message) } };
                response = _snsHandler.Publish(JsonConvert.SerializeObject(awsMessage), _topicArn, _options.Subject);
            }

            return response;
        }
    }
}
