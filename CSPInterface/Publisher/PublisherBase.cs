﻿using Amazon.Runtime;
using SNSWrapper.Interface.Model;
using SNSWrapper.Interface.Service;
using System.Collections.Generic;
using System.Linq;

namespace SNSWrapper.Publisher
{
    public abstract class PublisherBase : IPublisher
    {
        protected ISNSHandler _snsHandler = null;
        protected string _topicArn = null;
        protected IOptions _options = null;
        protected string[] _requiredParameters = null;
        public string[] RequiredParameters { get { return _requiredParameters; } }
        public IOptions Options { get { return _options; } }
        public PublisherBase(IOptions options, IArnGenerator arnGenerator, ISNSHandler snsHandler)
        {
            if (arnGenerator == null)
                _topicArn = string.Empty;
            else
                _topicArn = arnGenerator.Generate(options);

            _options = options;
            _snsHandler = snsHandler;
        }

        public abstract IAmazonWebserviceResponse Publish();
        public bool HasMissingParameters() 
        {
            if (_requiredParameters == null)
                return false;

            return _requiredParameters.Where(str => string.IsNullOrEmpty(str)).Any();
        }
    }
}
