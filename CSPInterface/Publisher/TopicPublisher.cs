﻿using Amazon.Runtime;
using Amazon.SimpleNotificationService.Model;
using SNSWrapper.Interface.Service;
using SNSWrapper.Interface.Model;
using SNSWrapper.Model;
using SNSWrapper.SNS;

namespace SNSWrapper.Publisher
{
    public class TopicPublisher : PublisherBase
    {
        public TopicPublisher(IOptions options, IArnGenerator generator, ISNSHandler snsHandler) : base(options, generator, snsHandler) 
        { 
            _requiredParameters = new string[] { _options.AccountId, _options.Region, _options.Subject }; 
        }
        public override IAmazonWebserviceResponse Publish()
        {
            IAmazonWebserviceResponse createResponse = null;
            using (_snsHandler)
            {
                if (!_snsHandler.Exists(_topicArn))
                {
                    createResponse = _snsHandler.Publish(null, null, _options.Subject);
                }
            }

            return createResponse;
        }
    }
}
