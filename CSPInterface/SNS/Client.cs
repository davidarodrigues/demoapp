﻿using Amazon;
using Amazon.SimpleNotificationService;
using Amazon.SimpleNotificationService.Model;
using SNSWrapper.Service;
using System;

namespace SNSWrapper.SNS
{
    public class Client : IDisposable
    {
        private IAmazonSimpleNotificationService AmazonClient;
        private string key = "AKIAITEJSHU5CD3X4YRQ";
        private string secret = "T+35eVhBkQHMfVZtqE0KB6ypCBQpoM1KV4Lytfccu6pxvtaJq+KiSBzTZliSu31D";
        private string awsRegion = "us-east-1";

        public Client()
        {

            RegionEndpoint region = RegionEndpoint.GetBySystemName(awsRegion);
            var utils = new CryptoUtil();
            AmazonClient = AWSClientFactory.CreateAmazonSimpleNotificationServiceClient(key, utils.Decrypt(secret), region);
        }

        public PublishResponse Publish(PublishRequest request)
        {            
            return AmazonClient.Publish(request);
        }

        public SubscribeResponse Subscribe(SubscribeRequest request)
        {
            return AmazonClient.Subscribe(request);
        }

        public ListTopicsResponse ListTopics(ListTopicsRequest request)
        {
            return AmazonClient.ListTopics(request);
        }

        public CreateTopicResponse CreateTopic(CreateTopicRequest request)
        {
            return AmazonClient.CreateTopic(request);
        }

        public ListSubscriptionsByTopicResult ListSubscriptions(ListSubscriptionsByTopicRequest request)
        {
            return AmazonClient.ListSubscriptionsByTopic(request);
        }

        public void Dispose()
        {
            AmazonClient.Dispose();
        }
    }
}
