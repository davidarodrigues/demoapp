﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SNSWrapper.Interface.Service;
using Amazon.Runtime;
using SNSWrapper.Model;
using SNSWrapper.Interface.Model;

namespace SNSWrapper.SNS
{
    public abstract class RequestBase : IDisposable
    {
        protected Client _client;
        protected IMapper<AmazonWebServiceResponse, SNSResponse> _mapper;
        public RequestBase(Client client, IMapper<AmazonWebServiceResponse, SNSResponse> mapper)
        {
            _client = client;
            _mapper = mapper;
        }

        public void Dispose()
        {
            _client.Dispose();
        }
    }
}
