﻿using Amazon.Runtime;
using Amazon.SimpleNotificationService.Model;
using SNSWrapper.Interface.Model;
using SNSWrapper.Interface.Service;
using SNSWrapper.Model;
using System.Collections.Generic;
using System.Linq;

namespace SNSWrapper.SNS
{
    public class TopicHandler : RequestBase, ISNSHandler
    {
        public TopicHandler(Client client, IMapper<AmazonWebServiceResponse, SNSResponse> mapper) : base(client, mapper) { }
        public bool Exists(string value)
        {
            ListTopicsRequest request = new ListTopicsRequest();
            return Exists(request, value);
        }

        private bool Exists(ListTopicsRequest request, string topicArn)
        {
            ListTopicsResponse response = _client.ListTopics(request);
            List<Topic> topics = response.Topics;
            string[] topicArns = topics.Select(t => t.TopicArn).ToArray();

            if (topicArns.Contains(topicArn))
                return true;

            if (!string.IsNullOrEmpty(response.NextToken))
            {
                request.NextToken = response.NextToken;
                return Exists(request, topicArn);
            }

            return false;
        }

        /// <summary>
        /// Publishes the specified subject.
        /// </summary>
        /// <param name="subject">The subject.</param>
        /// <returns></returns>
        public IAmazonWebserviceResponse Publish(string jsonMessage, string topicArn, string subject)
        {
            CreateTopicRequest request = new CreateTopicRequest(subject);
            AmazonWebServiceResponse response = _client.CreateTopic(request);
            SNSResponse snsResponse = new SNSResponse();
            _mapper.Map(response, out snsResponse);
            return snsResponse;
        }
    }
}
