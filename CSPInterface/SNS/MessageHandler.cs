﻿using Amazon.Runtime;
using Amazon.SimpleNotificationService.Model;
using SNSWrapper.Interface.Model;
using SNSWrapper.Interface.Service;
using SNSWrapper.Model;

namespace SNSWrapper.SNS
{
    public class MessageHandler : RequestBase, ISNSHandler
    {
        public MessageHandler(Client client, IMapper<AmazonWebServiceResponse, SNSResponse> mapper) : base(client, mapper) { }

        public IAmazonWebserviceResponse Publish(string jsonMessage, string topicArn, string subject)
        {
            IAmazonWebserviceResponse response = null;
            response = PublishJson(_client, jsonMessage, topicArn, subject);
            return response;
        }

        private IAmazonWebserviceResponse PublishJson(Client client, string jsonMessage, string topicArn, string subject)
        {
            // Publish Request
            PublishRequest request = new PublishRequest()
            {
                MessageStructure = "json",
                Message = jsonMessage,
                TopicArn = topicArn,
                Subject = subject
            };

            var response = client.Publish(request);
            var snsResponse = new SNSResponse();
            _mapper.Map(response,out  snsResponse);
            return snsResponse;
        }

        public bool Exists(string value)
        {
            throw new System.NotImplementedException();
        }
    }
}
