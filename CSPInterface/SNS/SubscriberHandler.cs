﻿using Amazon.SimpleNotificationService.Model;
using System.Collections.Generic;
using System.Linq;
using SNSWrapper.Interface.Service;
using Amazon.Runtime;
using SNSWrapper.Interface.Model;
using SNSWrapper.Model;

namespace SNSWrapper.SNS
{
    public  class SubscriberHandler : RequestBase, ISNSHandler
    {
        public SubscriberHandler(Client client, IMapper<AmazonWebServiceResponse, SNSResponse> mapper) : base(client, mapper) { }
        public IAmazonWebserviceResponse Publish(string topicArn, string endpoint, string protocol)
        {
            var request = new SubscribeRequest()
            {
                TopicArn = topicArn,
                Endpoint = endpoint,
                Protocol = protocol
            };
            var response = _client.Subscribe(request);
            var snsResponse =  new SNSResponse();
            _mapper.Map(response,out snsResponse);

            return snsResponse;
        }

        public bool IsSubscribed(ListSubscriptionsByTopicRequest request, string owner)
        {
            ListSubscriptionsByTopicResult result;

            result = _client.ListSubscriptions(request);
            IEnumerable<Subscription> subscriptions = result.Subscriptions.Where(s => s.Owner == owner && s.TopicArn == request.TopicArn);

            if (subscriptions.Any())
                return true;

            if (!string.IsNullOrEmpty(result.NextToken))
            {
                request.NextToken = result.NextToken;
                return IsSubscribed(request, owner);
            }
            return false;
        }


        public bool Exists(string value)
        {
            throw new System.NotImplementedException();
        }
    }
}
