﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SNSWrapper.Interface.Service;
using SNSWrapper.SNS;
using SNSWrapper.Interface.Model;
using Amazon.Runtime;
using SNSWrapper.Model;

namespace SNSWrapper.Service
{
    public class HandlerFactory : IHandlerFactory
    {
        private IOptions _options;
        private IMapper<AmazonWebServiceResponse, SNSResponse> _mapper;
        public HandlerFactory(IOptions options, IMapper<AmazonWebServiceResponse, SNSResponse> mapper)
        {
            _options = options;
            _mapper = mapper;
        }
        public ISNSHandler GetHandler()
        {
            ISNSHandler publisher = null;
            var client = new Client();
            switch (_options.Type)
            {
                case "topic":
                    publisher = new TopicHandler(client, _mapper);
                    break;
                case "message":
                    publisher = new MessageHandler(client, _mapper);
                    break;
                case "subscribe":
                    publisher = new SubscriberHandler(client, _mapper);
                    break;
                default:
                    publisher = null;
                    break;
            }
            return publisher;
        }
    }
}
