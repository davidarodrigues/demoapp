﻿using SNSWrapper.Interface.Model;
using SNSWrapper.Interface.Service;

namespace SNSWrapper.Service
{
    public class DefaultArnGenerator : IArnGenerator
    {
        public string Generate(IOptions options)
        {
            return string.Format("arn:aws:sns:{0}:{1}:{2}", options.Region, options.AccountId, options.Subject);
        }
    }
}
