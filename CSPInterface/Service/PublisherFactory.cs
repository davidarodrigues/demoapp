﻿using SNSWrapper;
using SNSWrapper.Interface.Model;
using SNSWrapper.Interface.Service;
using SNSWrapper.Publisher;
using SNSWrapper.SNS;

namespace SNSWrapper.Service
{
    public class PublisherFactory : IPublisherFactory
    {
        private IOptions _options;
        private IArnGenerator _arnGenerator;
        private IPublisher _helpPublisher;
        private ISNSHandler _snsHandler;
        public PublisherFactory(IOptions options, IArnGenerator arnGenerator, IPublisher helpPublisher, ISNSHandler snsHandler)
        {
            _options = options;
            _arnGenerator = arnGenerator;
            _helpPublisher = helpPublisher;
            _snsHandler = snsHandler;
        }
        public IPublisher GetPublisher()
        {

            if (!_options.AreValid())
                _options.Type = "help";

            IPublisher publisher = null;
            switch (_options.Type)
            {
                case "topic":
                    publisher = new TopicPublisher(_options, _arnGenerator, _snsHandler);
                    break;
                case "message":
                    publisher = new MessagePublisher(_options, _arnGenerator, _snsHandler);
                    break;
                case "subscribe":
                    publisher = new Subscriber(_options, _arnGenerator, _snsHandler);
                    break;
                default:
                    publisher = _helpPublisher;
                    break;
            }
            return publisher;
        }
    }
}
