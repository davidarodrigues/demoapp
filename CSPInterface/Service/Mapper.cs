﻿using System;
using System.Linq;
using System.Reflection;
using SNSWrapper.Interface.Service;

namespace SNSWrapper.Service
{
    public class Mapper<F, T> : IMapper<F, T>
    {
        /// <summary>
        /// Transforms the specified input.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="output">The output.</param>
        public void Map(F input, out T output)
        {
            output = (T)Activator.CreateInstance(typeof(T));

            PropertyInfo[] properties = output.GetType().GetProperties();
            PropertyInfo[] inputProperties = input.GetType().GetProperties();

            foreach (PropertyInfo property in properties)
            {
                var foundProperties = inputProperties.Where(p => p.Name == property.Name);
                if (foundProperties.Any())
                    property.SetValue(output, foundProperties.First().GetValue(input));
            }
        }
    }
}