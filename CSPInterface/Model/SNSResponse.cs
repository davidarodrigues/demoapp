﻿using Amazon.Runtime;
using SNSWrapper.Interface.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SNSWrapper.Model
{
    public class SNSResponse : IAmazonWebserviceResponse
    {
        public System.Net.HttpStatusCode HttpStatusCode { get; set; }

        public IDictionary<string, string> MetaData { get; set; }

        public SNSResponse(AmazonWebServiceResponse response)
        {
            HttpStatusCode = response.HttpStatusCode;
            MetaData = response.ResponseMetadata.Metadata ?? new Dictionary<string, string>();
        }

        public SNSResponse() 
        {
            MetaData = new Dictionary<string, string>();
        }
    }
}
