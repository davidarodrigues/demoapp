﻿using System;
using SNSWrapper.Interface.Model;
using NDesk.Options;

namespace SNSWrapper.Model
{
    public class Options : IOptions
    {
        #region Properties
        private string _type = null;
        public string Type { get { return _type; } set { _type = value; } }

        private string _region = null;
        public string Region { get { return _region; } }

        private string _accountId = null;
        public string AccountId { get { return _accountId; } }

        private string _subject = null;
        public string Subject { get { return _subject; } }

        private string _endpoint = null;
        public string Endpoint { get { return _endpoint; } }

        private string _protocol = null;
        public string Protocol { get { return _protocol; } }

        private string _deploymentName = null;
        public string DeploymentName { get { return _deploymentName; } }

        private string _urls = null;
        public string Urls { get { return _urls; } }

        private string _username = null;
        public string Username { get { return _username; } }

        private bool _showHelp = false;
        public bool ShowHelp { get { return _showHelp; } }

        private OptionSet _options;
        public OptionSet OptionSet { get { return _options; } }

        private bool _areValid = false;
        public bool AreValid() { return _areValid; }
        #endregion

        #region Constructor
        public Options(string[] args)
        {
            try
            {
                _options = new OptionSet() 
                {
                    { "t|type=", "-t: the {TYPE} of request (topic, (default) message, subscribe)", v => _type = v },
                    { "r|region=", "-r: the {REGION} of the topic. (default) us-east-1", v => _region = v },
                    { "a|accountId=", "-a: the {ACCOUNTID} of the topic. (default) 186148884772", v => _accountId = v },
                    { "s|subject=", "-s: the {SUBJECT} of the topic. (default) POC_SNS_TESTING", v => _subject = v },
                    { "e|endpoint=", "-e: the {ENDPOINT} of the subscriber. (default) https://int-csp.cloudsuite.infor.com/WSWebClient/SNSService.svc/deploymentdetails/sns", v => _endpoint = v },
                    { "p|protocol=", "-p: the {PROTOCOL} of the subscriber (default) https", v => _protocol = v },
                    { "d|deploymentName=", "-d: the {DEPLOYMENTNAME} of the deployment. (default) POC_SNS_DEPLOYMENT_TEST", v => _deploymentName = v },
                    { "u|urls=", "-u: the {URLS} of the deployment. (default) nosite@nowhere.com,siteunknown@somewhere.net", v => _urls = v },
                    { "n|username=", "-n: the {USERNAME} of the deployment. (default) My User", v => _username = v },
                    { "h|help=", "-h: show the help menu", v => _showHelp = v != null }
                };
                 _options.Parse(args);

                _type = _type ?? "message";
                _region = _region ?? "us-east-1";
                _accountId = _accountId ?? "186148884772";
                _subject = _subject ?? "POC_SNS_TESTING";
                _endpoint = _endpoint ?? "https://int-csp.cloudsuite.infor.com/CloudSuitePortal/DeploymentService.svc/deploymentdetails/sns";
                _protocol = _protocol ?? "https";
                _deploymentName = _deploymentName ?? "POC_SNS_DEPLOYMENT_TEST";
                _urls = _urls ?? "nosite@nowhere.com,siteunknown@somewhere.net";
                _username = _username ?? "My User";
                _areValid = true;
            }
            catch (Exception)
            {
                _areValid = false;
                return;
            }

        }
        #endregion
    }
}
