﻿using SNSWrapper.Interface.Model;
namespace SNSWrapper.Model
{
    public class SNSMessage : ISNSDeploymentMessage
    {
        public string DeploymentName { get; set; }

        public string Urls { get; set; }

        public string Username { get; set; }
    }
}