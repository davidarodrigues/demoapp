﻿using SNSWrapper.Interface.Model;

namespace SNSWrapper.Interface.Service
{
    public interface IArnGenerator
    {
        string Generate(IOptions options);
    }
}
