﻿using Amazon.SimpleNotificationService.Model;

namespace SNSWrapper.Interface.Service
{
    public interface IPublishHandler
    {
        CreateTopicResponse PublishTopic(string subject, string topicArn, string subscriberEndpoint, string endpointType);
        PublishResponse PublishMessage(string jsonMessage, string topicArn, string subject);
        SubscribeResponse Subscribe(string endpoint, string protocol, string topicArn);
    }
}
