﻿
namespace SNSWrapper.Interface.Service
{
    public interface IMapper<F, T>
    {
        void Map(F input, out T output);
    }
}
