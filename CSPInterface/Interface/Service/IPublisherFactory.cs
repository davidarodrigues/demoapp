﻿
namespace SNSWrapper.Interface.Service
{
    public interface IPublisherFactory
    {
        IPublisher GetPublisher();
    }
}
