﻿using Amazon.SimpleNotificationService.Model;

namespace SNSWrapper.Interface.Service
{
    public interface ISNSTopicHandler
    {
        bool Exists(ListTopicsRequest request, string topicArn);
        CreateTopicResponse Create(CreateTopicRequest request);
    }
}
