﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SNSWrapper.Interface.Service
{
    public interface IHandlerFactory
    {
        ISNSHandler GetHandler();
    }
}
