﻿using Amazon.SimpleNotificationService.Model;

namespace SNSWrapper.Interface.Service
{
    public interface ISNSSubscriber
    {
        SubscribeResponse Subscribe(string topicArn, string endpoint, string protocol);
    }
}
