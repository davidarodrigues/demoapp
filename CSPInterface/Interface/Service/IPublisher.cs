﻿using Amazon.Runtime;
using SNSWrapper.Interface.Model;

namespace SNSWrapper.Interface.Service
{
    public interface IPublisher
    {
        IOptions Options { get; }
        IAmazonWebserviceResponse Publish();
        bool HasMissingParameters();
    }
}
