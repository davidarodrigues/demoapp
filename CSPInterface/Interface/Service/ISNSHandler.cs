﻿using Amazon.Runtime;
using SNSWrapper.Interface.Model;
using System;

namespace SNSWrapper.Interface.Service
{
    public interface ISNSHandler : IDisposable
    {
        IAmazonWebserviceResponse Publish(string jsonMessage, string topicArn, string subject);
        bool Exists(string value);
    }
}
