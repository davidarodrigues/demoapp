﻿
namespace SNSWrapper.Interface.Model
{
    public interface ISNSDeploymentMessage
    {
        string DeploymentName { get; set; }
        string Urls { get; set; }
        string Username { get; set; }
    }
}
