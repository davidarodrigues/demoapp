﻿
namespace SNSWrapper.Interface.Model
{
    public interface IPublishRequest
    {
        string region { get; set; }
        string accountId { get; set; }
        string subject { get; set; }
    }
}
