﻿using NDesk.Options;
using System.Collections;

namespace SNSWrapper.Interface.Model
{
    public interface IOptions
    {
        string Type { get; set; }
        string Region { get; }
        string AccountId { get; }
        string Subject { get; }
        string Endpoint { get; }
        string Protocol { get; }
        string DeploymentName { get; }
        string Urls { get; }
        string Username { get; }
        bool ShowHelp { get; }
        OptionSet OptionSet { get; }
        bool AreValid();
    }
}
