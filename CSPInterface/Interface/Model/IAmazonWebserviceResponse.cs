﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SNSWrapper.Interface.Model
{
    public interface IAmazonWebserviceResponse
    {
        HttpStatusCode HttpStatusCode { get; set; }
        IDictionary<string, string> MetaData { get; set; }
    }
}
