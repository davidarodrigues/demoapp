﻿using System;
using Api.Formatter;
using Api.Interface;
using Api.Options;
using System.Collections.Generic;
using System.Collections.Specialized;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Api.Service;
using System.Linq;
using System.Collections;

namespace Api.Tests.Service
{
    [TestClass]
    public class RequestProcessorTest
    {
        [TestMethod]
        public void RequestProcessor_GetFormatters()
        {
            List<KeyValuePair<string, string>> queryString = new List<KeyValuePair<string, string>>()
            {
                new KeyValuePair<string, string>("body", "upper"),
                new KeyValuePair<string, string>("title", "lower")
            };

            IRequestProcessor processor = new RequestProcessor();

            IReportFormatter[] formatters = processor.GetFormatters(queryString);

            IReportFormatter bodyFormatter = formatters.Where(f => f.GetType() == typeof (BodyFormatter)).First();
            IReportFormatter titleFormatter = formatters.Where(f => f.GetType() == typeof(TitleFormatter)).First();

            if (bodyFormatter == null)
                Assert.Fail("Missing Body Formatter");
            else
                Assert.IsTrue(bodyFormatter.Options().BodyOption == ReportOptions.CaseOption.Upper);

            if (titleFormatter == null)
                Assert.Fail("Missing Title Formatter");
            else 
                Assert.IsTrue(titleFormatter.Options().TitleOption == ReportOptions.CaseOption.Lower);
        }
    }
}
