﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Api.Tests.MockData;
using Api.Interface;
using Moq;
using Api.Models;
using Api.Repository;
using Api.Service;
using Api.Framework;
using SNSWrapper.Interface.Service;
namespace Api.Tests.Service
{
    [TestClass]
    public class ServiceTests : BaseTest
    {
        private IRepository<ReportDbSet> _repository;
        private IMapper<ReportDbSet, IReport> _mapper;

        public ServiceTests()
        {
            _repository = Mock.Of<IRepository<ReportDbSet>>();
            _mapper = Mock.Of<IMapper<ReportDbSet, IReport>>();
        }

        [TestMethod]
        public void ReportService_ServiceGetById()
        {
            ResetReport();
            ReportDbSet dbReport = MockFramework.GenerateDbReport(1);
            IReport outReport = MockFramework.GenerateReport(1);

            Mock.Get(_mapper).Setup(m => m.Map(It.IsAny<ReportDbSet>(), out outReport)).Verifiable();
            Mock.Get(_repository).Setup(m => m.GetById(1)).Returns(dbReport);

            ReportService<ReportDbSet, IReport> service = new ReportService<ReportDbSet, IReport>(_repository, _mapper);

            IReport testReport = service.GetById(1);
            Assert.AreEqual(testReport.Id, 1);            
        }

        [TestMethod]
        public void ReportService_NullReportTest()
        {
            ResetReport();
            ReportDbSet dbReport = null;
            IReport nullReport = null;

            Mock.Get(_mapper).Setup(m => m.Map(It.IsAny<ReportDbSet>(), out nullReport)).Verifiable();
            Mock.Get(_repository).Setup(m => m.GetById(2)).Returns(dbReport);

            ReportService<ReportDbSet, IReport> service = new ReportService<ReportDbSet, IReport>(_repository, _mapper);

            IReport testReport = service.GetById(2);
            Assert.IsNull(testReport);
        }
    }
}
