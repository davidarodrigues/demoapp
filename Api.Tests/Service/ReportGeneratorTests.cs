﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Api.Options;
using Api.Interface;
using Api.Models;
using Moq;
using Api.Service;
using Api.Framework;

namespace Api.Tests.Service
{
    [TestClass]
    public class ReportGeneratorTests : BaseTest
    {
        private IReportFormatter[] _formatters;
        private IReportFormatter _bodyFormatter;
        private IReportFormatter _titleFormatter;
        private IService<ReportDbSet, IReport> _service;
        private string _originalBody;
        private string _originalTitle;

        public ReportGeneratorTests()
        {
            _service = Mock.Of<IService<ReportDbSet, IReport>>();
            _bodyFormatter = Mock.Of<IReportFormatter>();
            _titleFormatter = Mock.Of<IReportFormatter>();

            _formatters = new IReportFormatter[] { _bodyFormatter, _titleFormatter };
        }

        [TestMethod]
        public void ReportGenerator_Format()
        {
            ResetReport();
            Mock.Get(_service).Setup(s => s.GetById(It.IsAny<int>())).Returns(_report);
            Mock.Get(_bodyFormatter).Setup(f => f.Format(ref _report)).Callback(() => _report.Body = _report.Body.ToLower());
            Mock.Get(_titleFormatter).Setup(f => f.Format(ref _report)).Callback(() => _report.Title = _report.Title.ToLower());

            _originalBody = _report.Body;
            _originalTitle = _report.Title;            

            IReportGenerator generator = new ReportGenerator(_formatters, _service);
            generator.GenerateReport(1);

            Assert.AreNotEqual(_originalBody, _report.Body, false);
            Assert.AreNotEqual(_originalTitle, _report.Title, false);
        }
    }
}
