﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Api.Interface;
using Api.Models;
using Api.Tests.MockData;
using Api.Framework;
using Api.Service;
using SNSWrapper.Interface.Service;

namespace Api.Tests.Service
{
    [TestClass]
    public class DetailServiceTests : BaseTest
    {
        private IRepository<ReportDbSet> _repository;
        private IMapper<ReportDbSet, IReportDetail> _mapper;

        public DetailServiceTests()
        {
            _repository = Mock.Of<IRepository<ReportDbSet>>();
            _mapper = Mock.Of<IMapper<ReportDbSet, IReportDetail>>();
            IReportDetail outReport = MockFramework.GenerateReportDetails(1);
            Mock.Get(_mapper).Setup(m => m.Map(It.IsAny<ReportDbSet>(), out outReport)).Verifiable();
        }

        [TestMethod]
        public void DetailService_GetDetails()
        {
            ReportDbSet details = MockFramework.GenerateDbReport(1);

            Mock.Get(_repository).Setup(r => r.GetById(It.IsAny<int>())).Returns(details);

            IService<ReportDbSet, IReportDetail> service = new ReportService<ReportDbSet, IReportDetail>(_repository, _mapper);

            IReportDetail outDetails = service.GetById(1);

            Assert.AreEqual(outDetails.Id, details.Id);
            Assert.AreEqual(outDetails.Author, details.Author);
            Assert.AreEqual(outDetails.PageCount, details.PageCount);
        }
    }
}
