﻿using Api.Framework;
using Api.Interface;
using Api.Tests.MockData;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Api.Tests
{
    [TestClass]
    public class BaseTest
    {
        protected ReportDbSet _reportDbSet;
        protected IReport _report;

        protected void ResetReport()
        {
            _reportDbSet = MockFramework.GenerateDbReport(1);
            _report = MockFramework.GenerateReport(1);
        }

        [TestInitialize]
        public void TestInitialize()
        {
            _reportDbSet = MockFramework.GenerateDbReport(1);
            _report = MockFramework.GenerateReport(1);
        }
    }
}
