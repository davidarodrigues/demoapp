﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Api.Repository;
using Api.Framework;
using Api.Interface;

namespace Api.Tests.Repository
{
    [TestClass]
    public class RepositoryTests : BaseTest
    {
        protected IFramework<ReportDbSet> _framework;

        public RepositoryTests()
        {
            _framework = Mock.Of<IFramework<ReportDbSet>>();
        }

        [TestMethod]
        public void ReportRepository_RepositoryGetById()
        {
            ResetReport();

            Mock.Get(_framework).Setup(m => m.GetById(1)).Returns(_reportDbSet);
            ReportRepository<ReportDbSet> repository = new ReportRepository<ReportDbSet>(_framework);

            var testReport = repository.GetById(1);
            Assert.AreEqual(testReport.Id, 1);

            testReport = repository.GetById(2);
            Assert.IsNull(testReport);
        }
    }
}
