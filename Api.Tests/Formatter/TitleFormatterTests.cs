﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Api.Formatter;
using Api.Options;
using System.Globalization;
using Api.Interface;
using Moq;

namespace Api.Tests.Formatter
{
    [TestClass]
    public class TitleFormatterTests : BaseTest
    {
        private TitleFormatter _titleFormatter;
        private ReportOptions _options = new ReportOptions();
        private IFormatHelper _helper;

        public TitleFormatterTests()
        {
            _helper = Mock.Of<IFormatHelper>();
        }

        [TestMethod]
        public void TitleFormatter_UpperCaseTest()
        {
            string oldTitle = _report.Title;
            _options.TitleOption = ReportOptions.CaseOption.Upper;
            _titleFormatter = new TitleFormatter(_options, _helper);


            _titleFormatter.Format(ref _report);

            Assert.AreEqual(oldTitle.ToUpper(), _report.Title, false);
        }

        [TestMethod]
        public void TitleFormatter_LowerCaseTest()
        {
            string oldTitle = _report.Title;
            _options.TitleOption = ReportOptions.CaseOption.Lower;
            _titleFormatter = new TitleFormatter(_options, _helper);

            _titleFormatter.Format(ref _report);

            Assert.AreEqual(oldTitle.ToLower(), _report.Title, false);
        }

        [TestMethod]
        public void TitleFormatter_NoFormat()
        {
            string oldTitle = _report.Title;
            _options = new ReportOptions();
            _titleFormatter = new TitleFormatter(_options, _helper);

            _titleFormatter.Format(ref _report);

            Assert.AreEqual(oldTitle, _report.Title, false);
        }
    }
}
