﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Api.Formatter;
using Api.Options;
using System.Globalization;
using Api.Interface;
using Moq;

namespace Api.Tests.Formatter
{
    [TestClass]
    public class BodyFormatterTests : BaseTest
    {
        private BodyFormatter _bodyFormatter;
        private ReportOptions _options = new ReportOptions();
        private IFormatHelper _helper;

        public BodyFormatterTests()
        {
            _helper = Mock.Of<IFormatHelper>();
        }

        [TestMethod]
        public void BodyFormatter_UpperCaseTest()
        {
            string oldBody = _report.Body;
            _options.BodyOption = ReportOptions.CaseOption.Upper;
            _bodyFormatter = new BodyFormatter(_options, _helper);


            _bodyFormatter.Format(ref _report);

            Assert.AreEqual(oldBody.ToUpper(), _report.Body, false);
        }

        [TestMethod]
        public void BodyFormatter_LowerCaseTest()
        {
            string oldBody = _report.Body;
            _options.BodyOption = ReportOptions.CaseOption.Lower;
            _bodyFormatter = new BodyFormatter(_options, _helper);

            _bodyFormatter.Format(ref _report);

            Assert.AreEqual(oldBody.ToLower(), _report.Body, false);
        }

        [TestMethod]
        public void BodyFormatter_NoFormat()
        {
            string oldBody = _report.Body;
            _options = new ReportOptions();
            _bodyFormatter = new BodyFormatter(_options, _helper);

            _bodyFormatter.Format(ref _report);

            Assert.AreEqual(oldBody, _report.Body, false);
        }
    }
}
