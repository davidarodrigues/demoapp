﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Api.Formatter;
using Api.Interface;

namespace Api.Tests.Formatter
{
    [TestClass]
    public class FormatHelperTests
    {
        private IFormatHelper _helper;

        public FormatHelperTests()
        {
            _helper = new FormatHelper();
        }

        [TestMethod]
        public void FormatHelper_FormatProperCase()
        {
            var input = "this is a test of proper case";
            var expected = "This Is A Test Of Proper Case";
            var results = string.Empty;

            results = _helper.FormatProperCase(input);

            Assert.AreNotEqual(input, results, false);
            Assert.AreEqual(expected, results, false);
        }

        [TestMethod]
        public void FormatHelper_FormatDefault()
        {
            var input = "this is a test of default format. the beginning of each sentence. should be capitalized.";
            var expected = "This is a test of default format.  The beginning of each sentence.  Should be capitalized.";
            var results = string.Empty;

            results = _helper.FormatDefault(input);

            Assert.AreNotEqual(input, results, false);
            Assert.AreEqual(expected, results, false);

        }
    }
}
