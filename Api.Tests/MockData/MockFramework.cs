﻿using Api.Framework;
using Api.Interface;
using Api.Models;
using System;
namespace Api.Tests.MockData
{
    public class MockFramework
    {
        public static ReportDbSet GenerateDbReport(int id)
        {
            if (id != 1)
                return null;
            return new ReportDbSet()
            {
                Id = 1,
                Color = "White",
                Title = "tEsT rEpOrt",
                Body = "tEsT rEpOrt bOdY.",
                Author = "Doctor Eliot Doolittle",
                PublishedOn = new DateTime(1962, 1, 13),
                PageCount = 353
            };
        }

        public static IReport GenerateReport(int id)
        {
            if (id != 1)
                return null;
            return new Report()
            {
                Id = 1,
                Color = "Blue",
                Title = "rEpOrT OnE",
                Body = "tHiS iS RepoRt OnE BoDy."
            };
        }

        public static IReportDetail GenerateReportDetails(int id)
        {
            if (id != 1)
                return null;
            return new ReportDetail()
            {
                Id = 1,
                Title = "tEsT rEpOrt",
                Author = "Doctor Eliot Doolittle",
                PublishedOn = new DateTime(1962, 1, 13),
                PageCount = 353
            };
        }
    }
}
