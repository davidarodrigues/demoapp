﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SNSWrapper.Service;
namespace SNSWrapper.Tests.Service
{
    [TestClass]
    public class ArnGeneratorTest : BaseTest
    {
        [TestMethod]
        public void ArnGenerator_GenerateArn()
        {
            var generator = new DefaultArnGenerator();
            string arn = generator.Generate(_options);

            Assert.AreEqual(arn, arnMock);
        }
    }
}
