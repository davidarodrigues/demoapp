﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SNSWrapper.Interface.Service;
using SNSWrapper.Service;
using SNSWrapper.Model;
using Amazon.Runtime;
using Moq;
using System.Collections.Generic;
using SNSWrapper.SNS;

namespace SNSWrapper.Tests.Service
{
    [TestClass]
    public class HandlerFactoryTests : BaseTest
    {
        private IHandlerFactory _factory;
        private ISNSHandler _handler;
        private IMapper<AmazonWebServiceResponse, SNSResponse> _mapper = Mock.Of<IMapper<AmazonWebServiceResponse, SNSResponse>>();
        [TestMethod]
        public void HandlerFactory_GetHandler()
        {
            RunTest("topic", typeof(TopicHandler));
            RunTest("message", typeof(MessageHandler));
            RunTest("subscribe", typeof(SubscriberHandler));

            _options.Type = "not supported";
            _factory = new HandlerFactory(_options, _mapper);
            _handler = _factory.GetHandler();

            Assert.IsNull(_handler);
        }

        private void RunTest(string factoryType, System.Type expectedReturnType)
        {
            _options.Type = factoryType;
            _factory = new HandlerFactory(_options, _mapper);
            _handler = _factory.GetHandler();

            Assert.IsInstanceOfType(_handler, expectedReturnType);
        }

        private void GenerateMockMapper()
        {
            IMapper<AmazonWebServiceResponse, SNSResponse> mapper = Mock.Of<IMapper<AmazonWebServiceResponse, SNSResponse>>();
            SNSResponse mockresponse = new SNSResponse()
            {
                HttpStatusCode = System.Net.HttpStatusCode.OK,
                MetaData = new Dictionary<string, string>() { { "success", "converted" } }
            };
            Mock.Get(mapper).Setup(m => m.Map(It.IsAny<AmazonWebServiceResponse>(), out mockresponse));
        }
    }
}
