﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SNSWrapper.Interface.Service;
using SNSWrapper.Service;
using Moq;
using SNSWrapper.Interface.Model;
using SNSWrapper.Model;
using SNSWrapper.Publisher;

namespace SNSWrapper.Tests.Service
{
    [TestClass]
    public class PublisherFactoryTests : BaseTest
    {
        private IArnGenerator _generator;
        private IPublisher _helper;
        private IAmazonWebserviceResponse _response;
        private IPublisherFactory _factory;
        private ISNSHandler _handler;

        public PublisherFactoryTests()
        {
            _generator = Mock.Of<IArnGenerator>();
            _helper = Mock.Of<IPublisher>();
            _response = new SNSResponse();
            _handler = Mock.Of<ISNSHandler>();
        }

        [TestMethod]
        public void PublisherFactory_GetPublisher()
        {
            IPublisher publisher;

            // Test invalid options return.

            SetupTest("message", false, false);
            _factory = new PublisherFactory(_optionsMock, _generator, _helper, _handler);
            publisher = _factory.GetPublisher();

            Assert.AreEqual(_helper, publisher);

            // Test message publisher
            SetupTest("message", false, true);
            _factory = new PublisherFactory(_options, _generator, _helper, _handler);
            publisher = _factory.GetPublisher();

            Assert.IsInstanceOfType(publisher, typeof(MessagePublisher));

            // Test subscribe publisher
            SetupTest("subscribe", false, true);
            _factory = new PublisherFactory(_options, _generator, _helper, _handler);
            publisher = _factory.GetPublisher();

            Assert.IsInstanceOfType(publisher, typeof(Subscriber));

            // Test topic publisher
            SetupTest("topic", false, true);
            _factory = new PublisherFactory(_options, _generator, _helper, _handler);
            publisher = _factory.GetPublisher();

            Assert.IsInstanceOfType(publisher, typeof(TopicPublisher));

            // Test call for helper publisher
            SetupTest("helper", true, true);
            _factory = new PublisherFactory(_options, _generator, _helper, _handler);
            publisher = _factory.GetPublisher();

            Assert.AreEqual(_helper, publisher);
        }

        private void SetupTest(string publisherType, bool testHelper, bool optionsValid)
        {
            _options.Type = publisherType;
            Mock.Get(_generator).Reset();
            Mock.Get(_generator).Setup(g => g.Generate(It.IsAny<IOptions>())).Returns(arnMock);

            Mock.Get(_helper).Reset();
            Mock.Get(_helper).Setup(h => h.HasMissingParameters()).Returns(testHelper);
            Mock.Get(_helper).Setup(h => h.Publish()).Returns(_response);

            Mock.Get(_optionsMock).Reset();
            Mock.Get(_optionsMock).Setup(o => o.AreValid()).Returns(optionsValid);
        }
    }
}
