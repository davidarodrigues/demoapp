﻿using SNSWrapper.Interface.Model;
using SNSWrapper.Model;
using Moq;
using System.Collections.Generic;
using System.Linq;

namespace SNSWrapper.Tests
{
    public class BaseTest
    {
        protected IOptions _options;
        protected IOptions _optionsMock;        
        protected string arnMock;
        private Dictionary<string, string> _args = new Dictionary<string,string>();
        private string[] _argString;
        public BaseTest()
        {
            AddArg("a", "123_ABC");
            AddArg("r", "us-east-1");
            AddArg("s", "Unit Testing");
            AddArg("d", "Deployment Name");
            AddArg("u", "List of urls");
            AddArg("n", "User's Name");
            AddArg("e", "endpoint of request");
            AddArg("p", "protocol of endpoint");
            
            _argString = _args.Select(v => string.Format("-{0}{1}",v.Key, v.Value)).ToArray();
            _options = new Options(_argString);

            GenerateArn();

            _optionsMock = Mock.Of<IOptions>();
        }

        protected void SetOptionValue(string key, string value)
        {
            if (_args.ContainsKey(key))
                _args[key] = value;
            SetOptions();            
        }

        private void SetOptions()
        {
            _argString = _args.Select(v => string.Format("-{0}{1}", v.Key, v.Value)).ToArray();
            _options = new Options(_argString);
        }

        private void GenerateArn()
        {
            arnMock = string.Format("arn:aws:sns:{0}:{1}:{2}", "us-east-1", "123_ABC", "Unit Testing");
        }

        private void AddArg(string identifier, string value)
        {
            _args.Add(identifier, value);
        }
    }
}
