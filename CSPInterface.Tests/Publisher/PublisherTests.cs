﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SNSWrapper.Publisher;
using SNSWrapper.Interface.Service;
using Moq;
using SNSWrapper.Interface.Model;
using SNSWrapper.Model;
using System.Collections.Generic;

namespace SNSWrapper.Tests.Publisher
{
    [TestClass]
    public class PublisherTests : BaseTest
    {
        private IArnGenerator _generator;

        [TestMethod]
        public void MessagePublisher_Publish()
        {
            SetupTest();
            ISNSHandler handler = Mock.Of<ISNSHandler>();

            SNSResponse returnResponse = new SNSResponse()
            {
                HttpStatusCode = System.Net.HttpStatusCode.OK,
                MetaData = new Dictionary<string, string>() { { "success", "message published" } }
            };
            Mock.Get(handler).Setup(h => h.Exists(It.IsAny<string>())).Returns(false);
            Mock.Get(handler).Setup(h => h.Publish(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(returnResponse);
            var publisher = new MessagePublisher(_options, _generator, handler);

            IAmazonWebserviceResponse response = publisher.Publish();
            Assert.AreEqual(response.HttpStatusCode, returnResponse.HttpStatusCode);
            Assert.AreEqual(response.MetaData["success"], returnResponse.MetaData["success"]);
            Assert.AreEqual(response.MetaData["success"], "message published");
        }

        private void SetupTest()
        {
            _generator = Mock.Of<IArnGenerator>();
            Mock.Get(_generator).Setup(g => g.Generate(It.IsAny<IOptions>())).Returns(arnMock);
        }
    }
}
